Petri-Netze
===========

.. image:: images/CSP/Petri_ex1.png

Definionen
``````````

Vorbereich
    .. image:: images/CSP/Petri_vorbereich.png

Nachbereich
    .. image:: images/CSP/Petri_nachbereich.png

Konzession
    Konzession bedeutet, dass alle Stellen im Vorbereich
    einer Transition mindestens so viele Tokens besitzen wie das
    Kantengewicht der Kante, die diese Stelle mit der Transition verbindet
    und dass im Nachbereich der Transition keine der Stellen seine
    Kapazität überschreiten wird.
    Falls diese zwei Bedingungen erfüllt sind hat die Transition Konzession
    zum schalten.

.. image:: images/CSP/Petri_Semaphore.png

Tote Markierung = Deadlock (keine abgehenden Kanten im Erreichbarkeitsgraphen)

Petri Netz als Matrix
`````````````````````
.. image:: images/CSP/Petri_als_matrix.png

.. image:: images/CSP/Petri_invariante.png


Nachteile/Vorteile
``````````````````

Vorteile von Petri-Netze

- Grafisch gut darstellbar;
- Möglichkeit zur Analyse und Simulation;
- Solides theoretisches Fundament;
- Verschiedene Software-Tools zur Modellierung verfügbar;
- Weit verbreitetes Basiskonzept zur Modellierung kooperativer
  Prozesse.

Nachteile von Petri-Netze

- Höhere Petri-Netze sind schwer zu erstellen;
- Unübersichtlichkeit bei komplexen Netzen bei der Verwendung von low
  level Netzen;
- Petri-Netze besitzen eine statische Struktur (im Gegensatz zum p-
  Kalkül);
- Es gibt keine allgemeine Methode, wie man Petri-Netze erstellt.



CSP (Communicating Sequential Processes)
========================================


Prozess Algebra
---------------

LTS - Labelled Transition System
````````````````````````````````

Gerichteter Graph mit beschrifteten Kanten:

- Knoten = Zustände
- Kanten = Zustandsübergänge (Eingabe/Ausgabe, Ausgabe mit Balken drüber)
- es sind endliche und unenedliche Sätze möglich
    - darum sind LTS **keine** endlichen Automaten

.. image:: images/CSP/LTS_ex1.png


CSP Notation
````````````

Prozesse:

- Grossbuchstaben P, Q, R.

Ereignisse:

- Kleinbuchstaben x,y,z

Prozess-Sets

- Grossbuchstaben (vom Anfang des Alphabets): A, B, C.

Prozess-Alphabet

- Menge der Ereignisse, die einem Prozess zugeordnet sind:
- :math:`\alpha` SELECTA = {münze, badge, cola, kaffee, keks}.

Spezialfall:
- Der STOP Prozess (tut nichts; das System hält an): :math:`\omega`
- Der Prozess SKIP tut gar nichts. Danach ist nichts mehr möglich.

.. image:: images/CSP/LTS_Skip.png

Korrekte Terminierung
    Ein Prozess wird korrekt abgeschlossen, falls das System sich am
    Schluss im Zustand *SKIP* befindet


Beispiel
````````

*SELECTA = (coin -> ( choc -> ( coin -> (choc -> STOP ) ) ) )*


*CLOCK = (click -> COCK)* (rekursiv, unendlich ticken möglich)

:math:`\alpha` CH5 = {in5,out1,out2}

Wechseln von 5 in 2/1/2:  (in5 → out2 → out1 → out2 → CH5)

Wechseln von 5 in 1/1/1/2: CH5 = (in5 → out1 → out1 → out1 → out2 → CH5)


Alternative Ausführung [Choice]
```````````````````````````````

Notation
    (x -> P|y -> Q)

    Ein Objekt kann je nach Eintreffen des Ereignisses ( x oder y ) sich
    anschliessend wie der Folgeprozess ( P oder Q ) verhalten.


Traces
------

Definition
    Sei P ein Prozess.
    Dann bezeichnet man das Set aller (endlichen) Sequenzen von
    Ereignissen, welche von P ausgeführt werden können,
    als Traces von P.

Notation
     traces P =< a0, a1 ,...a n >

Beispiele
     <> : leere Trace
    < a >: Trace besteht nur aus dem Ereignis a.
    traces STOP = {<>}

- traces (a → b → STOP)
    - {<>, < a >, < a, b >}
- traces a → STOP b → STOP)
    - {<>, < a >, < b >}

Verknüpfung
    Die Verknüpfung der Traces s und t besteht aus s gefolgt von t.

    Notation: s ^ t

    < a, b, c > ^ < c, d, e > = < a, b, c, c, d, e >

Einschränkungen von Traces
    Falls ein Trace auf Ereignisse einer bestimmten Menge eingeschränkt
    werden soll, dann wird dies folgendermassen notiert:

    .. image:: images/CSP/traces_einschraenken1.png

    .. image:: images/CSP/traces_einschraenken_ex1.png

    .. image:: images/CSP/traces_einschraenken_regeln.png


Kommunikations-Events
---------------------

- Ausgabe Event: c!e
- Eingabe Event: c?e
- Kopieren des Wertes x aus der Eingabe in die Ausgabe
    - *Copy = in?x -> out!x -> Copy*

Channels und Events sind zwar zu unterscheiden, aber:

Channels sind einfach ein Set bestehend aus Events

Kanal a:{0,1} bestehend aus den Events {a:0, a:1}

Ab S. 59 Folien vergleichen!

Bismiulation
------------

sind zwei Systeme Bismimilär? -> meistens nicht :-P