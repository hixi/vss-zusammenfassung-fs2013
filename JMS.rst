Messaging
=========

Grundidee: **Loose Kopplung von Komponenten mittels Messaging**
--> Message basierte Middleware

- JMS als Beispiel (MQSeries, ... verwenden selbe Prinzipien)

- Wesentlich anderes Integrationsmodell als bei RPC /RMI:
    - Daten werden übermittelt
    - Ausführung muss daraus "etwas machen".

**Ausprägungen**:
- Synchron
- Asynchron
- Persistent / Nicht persistent
- Fehlertolerant

Integrationspatterns (bei Hohpe & Woolf ist fast alles ein Pattern!!!)

Kommunikationsmodelle: Punkt-zu-Punkt / PTP
...........................................

In diesem Kommunikationsmodell besitzt eine Message höchstens einen Empfänger:

- Der sendende Client (einer oder mehrere) adressieren die Message an eine
  Warteschlange Queue, welche die Nachrichten für den beabsichtigten
  (empfangenden) Client aufgewahrt.
- Eine Warteschlange ist also als Mailbox / Briefkasten zu verstehen.

.. image:: images/JMS/MQ_PTP_modells.png

Kommunikationsmodelle: Publish-and-Subscribe
............................................

Im Gegensatz zum Punkt-zu-Punkt Modell gestattet das Pub/Sub Modell
Nachrichten an mehrere Clients abzuliefern.

- Der sendende Client adressiert (publiziert) die Message an ein Topic.
- Mehrere Clients können sich bei einem Topic eintragen.

.. image:: images/JMS/MQ_publish_subscribe_modells.png

Programmiermodell
-----------------

+----------------------------------------------------------------------+--------------------------------------------------------------+
| .. image:: images/JMS/MQ_prog_modell_schemata.png                    |  .. image:: images/JMS/MQ_prog_modell_schemata_proxy.png     |
+----------------------------------------------------------------------+--------------------------------------------------------------+
|     Message Queue Programmiermodell Schema                           |          Message Queue Programmiermodell Schema              |
+----------------------------------------------------------------------+--------------------------------------------------------------+


**Producer:**

- Sendet eine Nachricht an die Warteschlange: send();


**Receiver:**

- Empfängt die Nachricht, indem er sie aus der Warteschlange liest
- Drei Lösungsansätze:
    - Blockierendes Lesen:
        - Der Receiver wird solange blockiert, bis die Nachricht
          in der Warteschlange ist.
    - Nicht-blockierendes Lesen: polling
        - Überprüfen der Warteschlange, ob eine Nachricht eingetroffen ist.
        - Lesen der Nachricht oder einer Meldung, dass nichts da ist.
    - notify():


**Messaging gestattet eine einfache lose Kopplung unterschiedlichster Systeme**

- Auf unterschiedlichen Plattformen
- In unterschiedlichen Programmiersprachen
- Mit völlig unterschiedlichen Message-Formaten (Text, Byte, Objekt).

**Messaging wird heute vielfach als einfacherer Ersatz für die Integration unterschiedlicher Systeme eingesetzt**:

- Einfachheit
- Lose Kopplung
- Erweiterbarkeit
- Skalierbarkeit
- Fehlertoleranz

sind einige der typischen Merkmale von MOM.


**Der Nachrichtenaustausch erfolgt in der Regel mittels eines der zwei Kommunikations-Patterns**:

- Point to Point (P2P)
- Publish / Subscribe

**Nachrichten können sowohl persistent als auch nicht-persistent verwaltet werden**.


JMS (Java Message Service)
--------------------------

siehe auch :ref:`messaging_src`.

In JMS wird festgelegt, dass eine Message aus folgenden Teilen besteht:

- Header
- Properties (Erweiterungen des Headers)
- Body

Ein **Header** wird immer verlangt. Er enthält Informationen für das
Routing und die Identifikation (weitere Details finden Sie weiter unten).

**Properties** sind optional - sie enthalten zum Beispiel Informationen, mit
deren Hilfe ein Client Nacrichten filtern kann.

Der **Body** ist auch zwingend erforderlich - er enthält die aktuellen
Daten (Text, Objekte,...).


Message Selector
................

Ein JMS Message Selector erlaubt es einem Client mit Hilfe des
Message Headers zu spezifizieren, an welchen Messages er
interessiert ist.

- Nur Nachrichten, deren Header und Properties dem Selektor
  entsprechen, werden abgeliefert.

- Dabei muss der Begriff "nicht abgeliefert" geklärt werden:
    - es werden nur jene Nachrichten akzeptiert, bei denen die
      Header und Property Felder mit jenen des Selektors
      identisch sind.


.. image:: images/JMS/JMS_Factories.png


Distributed Shared Memory
-------------------------

(Distributed) Shared Memory wird vor allem dann eingesetzt, wenn
mehrere Prozesse die selbe oder eine ähnliche Aufgabe erledigen
müssen.

- Parallelisierungs-Anwendungen
- Multiprozessor-Anwendungen

.. image:: images/JMS/Shared_memory.png


Tupel Spaces
------------

Programmmiermodell
..................

 Ziel des Ansatzes war es eine Möglichkeit zu bieten asoziativ in
einem Speicher zu suchen.

- Der Zugriff erfolgt nicht über Verzeichnisse auf Dateisystemen.
- Tupel-Systeme verwenden lediglich wenige, aber mächtige Befehle

.. image:: images/JMS/tupel_spcaces.png

JavaSpaces
..........

Verteilter, gemeinsam genutzter „dynamischer Speicher“

- hilft ein Netzwerk von (J)VMs zu föderieren
- erlaubt eine einfache dynamische Persistenz / Speicherung
- basiert auf dem verteilten Transaktionsprotokoll
- benutzt verteilte Lease und Ereignis-Protokolle


JavaSpaces Programmiermodell
............................

Programmierung Verteilter Systeme:

- Austausch von Nachrichten
- direkte Kommunikation von Prozessen
- Kontrolle konkurrierender Zugriffe von Clients
- verlässliche Speicherstrategie
- verteiltes Transaktionsprinzip (2PC=two phase commit)
- Aufruf entfernter Methoden (RMI)
- komplexe Programmierung


Service stellt einen Speicherbereich (Spaces) für Objekte bereit

- Prozesse kommunizieren nicht direkt miteinander
- Objekte werden zwischen den Services ausgetauscht
- Prozess kann Objekt in den Space schreiben
- im Speicherbereich sind nur passive Objekte


**Eigenschaften von Spaces**

- mehrere entfernte Prozesse können mit einem Space kommunizieren
- Spaces sind persistent
- Spaces sind assoziativ
- Transaktionen sind safe
- Spaces erlauben den Austausch ausführbarer Inhalte

**Elementare Operationen von Spaces**

- write()
- read()
- take()
- notify()


JavaSpaces speichern Objekte

zur Nutzung der Objekte müssen diese:

- gelesen werden: eine Kopie wird erstellt (read() )
- aus dem Space entfernt werden: (take() )
- ein Nutzer kann den Eventdienst von Jini nutzen, um benachrichtigt zu
  werde, sobald ein Objekt eingelagert wird, welches den „Interessen“
  des Nutzers entsprechen
