RMI (Remote Method Invocation)
==============================

Generelles
----------

Das verteilte Objektsystem sollte sich möglichst gut in das bestehende
Objektsystem einfügen:

- Einfach einsetzbar
- Keine exotischen neuen Sprachkonzepte.


Ziele von RMI (Remote Method Invocation) in Java:

- Untersützung für unterschiedliche VMs (IBM, Sun, ...)
- Security!
- Es soll sichtbar sein, ob eine Methode lokal oder remote einsetzbar ist.
- Typensicherheit
- Unterschiedliche Referenz-Semantiken:
    - ...
    - Lazy Activation

**Remote Procedure Calls (RPC)**

- Sind die Erweiterung der Prozeduraufrufe zum entfernten
  Prozeduraufruf.
- Ermöglicht, dass die Clients Servern Prozeduren aufrufen.

**Objektorientierte Remote Methodenaufrufe (RMI)**

- Sind Erweiterungen der Methodenaufrufe zum entfernten
  Methodenaufruf (I steht für Invocation).
- Java RMI ist eine Beispielimplementierung.

**Ereignisbasierte Programmierung wird um verteilte Ereignisse**
**erweitert (Beispiel: Jini).**

- Objekte benachrichtigen andere Objekte mithilfe eines
  Nachrichtenaustausches über ein Netzwerk.

+-----------------------------------------------+---------------------------------------------+
| .. image:: images/RMI/prozedur_vs_methode.png | .. image:: images/RMI/generelles_modell.png |
+-----------------------------------------------+---------------------------------------------+

Entwicklung einer RMI Aplikation
--------------------------------

**RMI ist ein Beispiel für die klassische Client Server Architektur.**

- Server bietet Services, publiziert über Interface
- Service wird in eine Registry eingetragen
- Client bestimmt Service über Registry und nutzt anschliessend den Service.

**Ausprägungen**:

- Klassisch:
    - Client und Server verwenden gemeinsames Interface, Ablauf wie oben.
- Dynamisch:
    - benötigte Klassen werden dynamisch geladen (typischerweise URL).
- Callbacks versus Polling:
    - Client kann sich beim Server registrieren.
- Activation: lassen wir weg.


Vorteile/Nachteile resp Einsatzgebiete von RMI
----------------------------------------------

Vorteil von RMI
...............

- Der Client benötigt nur den (Platzhalter) Namen des remote Objekts.
- Die Kommunikation unterscheidet sich aus Client Sicht kaum von einem
  lokalen Prozedur / Methodenaufruf
- Client und Server müssen sich lediglich über ein (gemeinsam
  genutztes) Interface einigen.

Einsatzgebiete
..............

**RMI macht nur Sinn, wenn Sie eine reine Java Umgebung vor sich haben**

Falls Sie heterogene Systeme

- Unterschiedliche Betriebsysteme
- Unterschiedliche Programmiersprachen
- Unterschiedliche Hardware

vor sich haben, sind Sie in der Regel gezwungen entweder CORBA
(Common Objekt Request Broker) oder Message basierte Middleware (MOM)
einzusetzen.


Registry/Server
...............

+--------------------------------------------+--------------------------------------------+
| .. image:: images/RMI/registry_einsatz.png | .. image:: images/RMI/generelles_modell.png|
+--------------------------------------------+--------------------------------------------+

**Die Registry ist ein Lookup / Namens-Dienst und wird typischerweise**
**als Standalone Service gestartet**

Genauer gesagt:

Starten der RMI Registry

- In der Regel wird diese als Standalone Programm (im \bin
  Verzeichnis von Java) gestartet.
- Sie muss im selben Verzeichnis gestartet werden, in dem sich die
  Server Klassen befinden, sonst muss man die Codebase (als VM
  Parameter) angeben.

Die Registry hat die Funktion eines „Lookup-Servers“:

- Der Server trägt die verfügbaren remote Objekte (Servants) dort
  unter einem festgelegten Namen ein.
- Der Client erhält darüber Zugriff auf das remote Objekt

Der Server muss nun nur noch das Proxy Objekt, den Service, in die
Registry (Hashtabelle) eintragen ("gebunden").

- Die Methode bind() kann genau einmal ausgeführt werden.
- Die Methode rebind() ist universeller:
- Falls das Objekt bereits registriert wurde, wird der Eintrag überschrieben;
- Sonst wird der Eintrag angelegt.
- Zum Testen kann man anschliessend die Registry abfragen und kontrollieren,
  ob der Eintrag korrekt erfolgte.

+----------------------------------------------+----------------------------------------------------+
| .. image:: images/RMI/proxy_and_skeleton.png | .. image:: images/RMI/registry_klassendiagramm.png |
+----------------------------------------------+----------------------------------------------------+

