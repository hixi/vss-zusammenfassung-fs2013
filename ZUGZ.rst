Zeit und globale Zustände
=========================

Das **Fehlen einer globalen Zeit** ist eines der fundamentalen Merkmale
verteilter Systeme.

- Allerdings können Uhren recht genau synchronisiert werden.

**Logische Uhren** gestatten eine Ordnung der Ereignisse, auch ohne Referenz auf eine
absolute Zeit:

- „**Happened-before**“ Relationen ermöglichen die Definition einer Ereignis-
  Halbordnung, aus der Kausalität folgt.

**Globale Zustände** umfassen sowohl Prozesse-Zustände als auch Kommunikationskanäle-
Zustände.

- Der Snapshot Algorithmus von Chandy und Lamport löst das Problem grundsätzlich;
- Einfachere und schnellere Algorithmen gestatten u.a. die Entdeckung von Deadlock
  Situationen.


Logische Uhren
--------------

Konzept einer logischen Uhr, bzw. einer logischen Zeit.

- Ordnung der Ereignisse in relativer Lage (statt absoluter Lage)
- bis auf Instruktionsgranularität möglich (Reihenfolge der Instruktionen).
- Skalierung der Zeitachse ist beliebig,
- nur die Ereignisse, für die eine Zeitaussage notwendig ist, werden
  berücksichtigt.

Eigenschaften der logischen Zeit

- Kausale Abhängigkeiten sollen korrekt berücksichtigt werden
- Ereignisse aus unabhängigen Abläufen müssen nicht relativ
  zueinander geordnet werden.

Die Lösung dafür (relative Lage):
- Wenn zwei Ereignisse im selben Prozess stattfinden, dann fanden sie
  in der Reihenfolge ihrer Beobachtung statt.
- Wenn eine Nachricht zwischen zwei Prozessen ausgetauscht wird,
  dann ist das Sendeereignis immer vor dem Empfangsereignis.

.. image:: images/zugz/schema.png

Lamport Algorithmus
...................

.. image:: images/zugz/lamport_algo.png


Vektor Uhren
------------

.. image:: images/zugz/vektor_uhr.png


Globale Zustände
----------------

**Die Bestimmung des globalen Zustandes ist in verteilten Systemen**
**eine schwierige Aufgabe.**

**Snapshot Algorithmen liefert nicht genug Informationen, um den**
**globalen Zustand zu bestimmen.**


Die Bestimmung eines globalen Zustandes ist eng gekoppelt mit der
Frage nach der globalen Zeit.

- Da wir lediglich mit der Lamport Halbordnung arbeiten wollen, müssen
  wir ein Konzept für einen globalen Zustand entwickeln, welches
  konsistent mit dem Lamport (oder eventuell allgemeiner mit dem
  Vektorzeit-) Modell ist.
- Die lokalen Zustände der Prozesse pi bilden den einen Teil zur
  Festlegung des globalen Zustandes;
- Die andere Hälfte beschreibt die Kommunikationskanäle bzw. die
  Nachrichten in den Kanälen.


.. image:: images/zugz/gloable_zustaende_wichtige_rolle.png

.. image:: images/zugz/schnitte_gloabler_zustand.png

- Der Schnitt 1 ist inkonsistent, weil das Empfangen der Nachricht m1 im
  Prozess 2 festgehalten wird;
  das Senden wird aber nirgends festgehalten.
  Wir haben also eine Nachricht, welche in einem globalen Zustand als
  empfangen, aber nicht als gesendet erkannt wurde.
  Die Grenze von Schnitt 1 besteht aus e10 und e20.

- Der Schnitt 2 ist konsistent, weil lediglich das Senden der Nachricht m2
  festgehalten wird; das Empfangen geschieht später.


Globales Zustandsattribut (engl. global state predicate):

- Funktion, welche einem globalen Zustand einen der Werte true oder
  false zuordnet.

Anforderung an ein globales Zustandsprädikat: was ist
wünschenswert?

- Stabilität
    - Wenn das System einmal den Zustand true erreicht hat, bleibt es
      in diesem.
- Safety (Korrektheit)
    - Wenn das System sich am Anfang nicht in einem unerwünschten
      Zustand befindet, wird es einen solchen nie erreichen.
- Liveness (Vollständigkeit)
    - Vereinfacht:
      Wenn eine Eigenschaft für den Prozess pi gilt,
      dann gilt sie auch für einen Teilprozess.

Chandy and Lamport Algorithmus
..............................

- Der Algorithmus nimmt für sich **nicht** in Anspruch einen
  physikalischen Zustand zum Zeitpunkt t zu beschreiben!
- Das Ziel des Algorithmus ist es, einen "Systemzustand" zu
  beschreiben, welcher bestimmbar und reproduzierbar ist

Konsequenz:
- Der Algorithmus hält einen Zustand fest, welcher physikalisch eventuell
  nie erreicht wird.
- Aber der mit dem Algorithmus bestimmte Zustand ist konsistent.

Skizzenhaft
```````````

p sendet eine Marker Nachricht an alle ausgehenden Kanäle,
sobald p seinen eigenen Zustand erfasst hat.

- Anschliessend kann p weitere Nachrichten versenden.

Danach werden alle Nachrichten, welche über eingehende Kanäle
eintreffen, registriert. Der Startzustand wird als leer angesehen.
Eintreffende Nachrichten werden dem Kanazustand zugeordnet.

Falls p eine Marker Nachricht über den Kanal c empfängt,
dann wird dem Kanal c die Menge der Nachrichten zugeordnet,
welche seit der Aufnahme des eigenen Zustands über den Kanal c
eingetroffen sind (ohne die Marker Nachricht).


.. figure:: images/zugz/chandy_lamport_algo_1.png

   ausgangslage

.. figure:: images/zugz/chandy_lamport_algo_2.png

   q erfasst seinen Zustand Sq1

.. figure:: images/zugz/chandy_lamport_algo_3.png

   p erfasst seinen Zustand als Sp2

.. figure:: images/zugz/chandy_lamport_algo_4.png

   q empfängt m3; m3 ist der Kanalzustand (in-Kanal)

   m3 fehlt in p bei dessen Zustandsaufname

   m3 fehlt in q bei dessen Zustandsaufname

   m3 ist "in transit" (im Kanal)



.. figure:: images/zugz/chandy_lamport_algo_5.png

   **Message in Transit: m3**

   Globaler Zustand = ((Sp2, Sq1), (0,m3) )



.. figure:: images/zugz/chandy_lamport_algo_6.png

   Globaler Zustand = ((Sp2, Sq1), (0,m3) )

   Aber: der globale Zustand wird im
   Gesamtablauf eventuell nie erreicht.



**Warum ist dieser Zustand Konsistent?**

Wir müssen einfach zeigen, dass immer wenn recv(m) erfasst wird,
dann muss auch send(m) erfasst worden sein.

.. image:: images/zugz/chandy_lamport_algo_konsist.png

**Der erfasste Zustand ist einer der möglichen konsistenten Zustände!**


Eigenschaften von global State
..............................

Falls Si und Sf den globalen Zustand des Systems zu zwei
unterschiedlichen Zeitpunkten beschreiben,
Si ist der Systemzustand bei der Zustandaufnahme
S f ist der Systemzustand beim Abschluss der Zustandsaufnahme
und S * der Zustand ist, den man mithilfe des Algorithmus erfasst,
dann gilt:

- S* ist erreichbar von Si
- Sf ist erreichbar von S*

**Der Chang Lamport Zustand S*** **ist sozusagen ein Zustand zwischen**
**zwei physikalisch realen Zuständen.**


Wahlen und Konsensfindung
-------------------------

Konnektivität
`````````````

- Kann asymmetrisch sein:
    - Die Kommunikation von pi zu pj kann möglich,
      gleichzeitig aber die Kommunikation von pj zu pi unmöglich
      sein.
- Kann intransitiv sein:
    - Die Kommunikation von pi zu pj und von pj zu pk können
      möglich sein, ohne dass pi mit pk direkt kommunizieren kann.

Ausfallverhalten
````````````````

- Ein Prozess pi kann nur ausfallen, wenn er abstürzt.
- Zufällige Fehler (sogen. Byzantinische Fehler) behandeln wir
  speziell.

Kritischer Bereich/Abschnitt
````````````````````````````

- Zeitabschnitt, während dem mehrere Prozesse auf eine
  gemeinsame Ressource zugreifen möchten
  (anschaulich: die Verkehrskreuzung).

**Lösungsansatz lokal:**

- Gemeinsam genutzte Variablen (Semaphore)
- Monitore (signal(), wait(), Warteschlangen)

**Lösungsansatz im Netzwerk:**

- Mithilfe von Nachrichtenaustausch

.. image:: images/zugz/wuk_verteilte_synch.png

(a)->Anfrage von p1 an Koordinator (p3): ist CS (critical Section) frei?
Der Koordinator gibt die CS frei.

(b)->Prozess p2 bittet den Koordinator um Freigabe der CS
Der Koordinator antwortet nicht und steckt p2 in die Warteschlange

(c)->p1 gibt die CS frei und informiert den Koordinator.
Der Koordinator informiert den ersten der Queue und gibt damit die CS
frei (Beispiel aus Tanenbaum: Verteilte Systeme)


Verteilter wechselseitiger Ausschluss
.....................................

Wie können Zugriffskonflikte beim Zugriff auf
gemeinsame Resourcen vermieden werden?

Prozesse werden mithilfe von zuverlässigen Kanälen
verbunden.

- Fehlende oder defekte Nachrichten werden erneut übermittelt
- das Protokoll ist dafür zuständig und implementiert diese
  Fähigkeit.
- Nachrichten werden innerhalb einer sinnvollen Zeitspanne
  übermittelt (Kanäle funktionieren).
- Ein Netzwerk kann durch den Ausfall eines Knotens
  (beispielsweise eines Routers) in mehrere selbständige,
  weiter funktionierende Teilnetze aufgeteilt werden, sogen.
  Partitionen


Applikationsprotokoll
`````````````````````

enter()
    Eintritt in einen kritischen Bereich (ggf. blockieren)

resourceAccess()
    Zugriff auf gemeinsam genutzte
    Ressourcen im kritischen Bereich

exit()
    Verlassen des kritischen Bereichs


Anforderungen (Mutual Exclusion: ME)
````````````````````````````````````

ME1 : Sicherheit
    Höchstens ein Prozess kann den kritischen Prozess-Bereich
    (Critical Sector, CS) gleichzeitig ausführen.

ME2 : Liveness + Fairness
    Anfragen eines Prozesses, den CS zu betreten sind irgendwann
    erfolgreich (kein Aushungern [starvation] /
    kein Verklemmen [deadlock])

ME3 : Reihenfolge + Fairness
    Die Prozesse können in der Reihenfolge den CS ausführen, wie
    sie die Anforderung stellen (überholen gibt es nicht).

Leistungsbewertung der Ausschlussalgorithmen
````````````````````````````````````````````

- Verbrauchte Bandbreite für die Koordination
    - Proportional zur Anzahl Nachrichten in den *enter()*, *exit()*
      Operationen (wie viele Nachrichten brauchen wir für die
      Koordination).
- Client-Verzögerung für Ein- und Austritt
    - durch die *enter()*, *exit()* Operationen
- Systemdurchsatz-Auswirkung
    - Verzögerungen durch Koordination:
      t.exit(i) – t.enter(i+1)

Lösungsvarianten
````````````````

- Mithilfe eines zentralen Servers
- Mithilfe eines Ring-basierten Algorithmus

.. figure:: images/zugz/wuk_ausschluss_token.png

   gegenseitiger Ausschluss-Token für ein Set von Prozessen

.. figure:: images/zugz/wuk_ausschluss_token_ring.png

   Token für den wechselseitigen Ausschluss

   Zugriff der Prozesses p i auf die CS
   geschieht der Reihe nach (mod(n) ).
   Falls ein pi keinen Zugriff anstrebt, gibt
    er den Token an den nächsten Prozess
   weiter.

   **Nachteil**: hoher Bandbreitenbedarf
   auch wenn niemand CS betreten
   möchte.

Token-Ring Bewertung
````````````````````

Sicherheit und Lebendigkeit sind erfüllt:

- Sicherheit ist leicht zu sehen: Nur ein Prozess hat das/den
  Token zur selben Zeit.
- Kein Prozess wird ausgehungert, da die Reihenfolge durch
  den Ring bestimmt ist.

Die Happened-before Relation ist **verletzt**: Maximal muss
ein Prozess warten, bis alle anderen Prozesse einmal im
kritischen Bereich waren.

Verlorene Token erfordern Neugenerierung durch Koordinator.

- Verlust eines Tokens ist schwer zu erkennen, da es sich
  auch um einen sehr langen Aufenthalt in einem kritischen
  Bereich handeln kann.



.. figure:: images/zugz/wuk_multicast_synch.png

   Multicast Synchronisation Beispiel

Verteilte Lösung Bewertung
``````````````````````````

Alle drei Basisbedingungen werden erfüllt. 
Aber:

1. Der single-point-of-failure wurde ersetzt durch n points-of-
   failure. Wenn ein Prozess (mit Warteschlange) nicht mehr
   arbeitet, funktioniert das System nicht mehr.
   - Verbesserung: Dieses Problem könnte durch explizite
   Verwendung eines Request-Reply-Protokolls ersetzt werden
   (jede Nachricht wird sofort bestätigt). Wenn keine Bestätigung
   kommt, ist der Prozess nicht mehr aktiv.
   
2. Jeder Prozess muss immer bei der Entscheidung mitwirken,
   obwohl er evtl. gar kein Interesse an der kritischen Region hat.
   - Verbesserung: eine einfache Mehrheit genügt.
   - Der Algorithmus ist insgesamt langsam, kompliziert, teuer
     und wenig robust, aber, wie A.S. Tanenbaum sagt: „Finally, like
     eating spinach and learning Latin in high school, some things
     are said to be good for you in some abstract way.”

Prozessabsturz
``````````````

- Zentrale Lösung: Absturz eines Clients, der das Token weder
  besitzt noch angefordert hat, kann toleriert werden.
- Verteilte Lösung: Kann Absturz tolerieren, wenn Request-Reply-
  Protokoll verwendet wird.
- Token-Ring: kein Prozess darf abstürzen


Wahlen
......

In vielen verteilten Algorithmen benötigt man einen Prozess, der eine
irgendwie geartete besondere Rolle spielt, z.B. als Koordinator,
Initiator oder Monitor.

Die Aufgabe eines Wahl-Algorithmus (Election-Algorithmen) ist es,
einen Prozess unter vielen gleichartigen Prozessen durch eine
kooperative, verteilte Wahl eindeutig zu bestimmen, der diese Rolle
übernimmt.

Anwendungsbeispiele:

- wechselseitiger Ausschluss
- Ausfall eines Koordinators

Ring basierte Wahl
``````````````````

Ziel

- Wahl des sogen. Koordinators (max Prozess-ID)

Vorgehen

- Jeder Prozess ist zuerst Nichtwähler.
- Jeder Prozess kann eine Wahl veranlassen.
- Fall ein Prozess eine Wahl initialisieren will oder muss, sendet er
  eine election (Wahl-) Nachricht an seinen Nachbarn. Diese enthält
  seine Prozess ID.
- Empfänger:
    - ist die Wahl-Prozess-ID grösser als seine
      dann sendet er die Nachricht weiter.
    - falls kleiner, dann trägt er seine Prozess-ID ein und wird

Wahlteilnehmer
- sonst (Prozess-ID auf dem Wahlzettel = eigene Prozess-ID)
  Prozess wird zum Koordinator

Probleme
- Fehler-Intoleranz und worst case: O(N)
  (der Ring-Vorgänger ist Koordinator: alle werden besucht)


Bully Algoritmus
````````````````

A-Priori Wissen:
- jeder Prozess weiss, welche Prozesse eine höhere Prozess-ID
  besitzen
  und jeder Prozess kann mit jedem andern Prozess kommunizieren.

Algorithmus:
- Wenn ein Prozess feststellt, dass der augenblickliche Koordinator nicht
  mehr reagiert, startet er den Auswahlprozess:
- P schickt eine ELECTION Nachricht an alle Prozesse mit höherer
  Nummer.
- Bekommt er keine Antwort, ist er der neue Koordinator.
- Bekommt er eine Antwort, ist seine Aufgabe erledigt:
- Der Antwortende übernimmt seine Arbeit!


.. figure:: images/zugz/wuk_wahl_bully.png

   bully wahl beispiel
   
Bewertung:
- hohe Nachrichtenkomplexität
    - worst case O(n2) Nachrichten.
    - best case (n - 2) Nachrichten
- alle Prozesse mit ihren Identifikatoren müssen allen
  bekannt sein.

Konsens und verwandte Probleme
..............................

Problemstellungen:
- Konsensfindung
- Byzantinische Generäle (Konsens bei Anwesenheit von Fehlern)
- Interaktive Konsistenz

Anwendungsbeispiel:
- Wenn Rechner eins bis drei einer Rakete das Ergebnis A, der vierte bis
  sechste das Ergebnis B für ein und die selbe Aufgabe liefern, muss
  entschieden werden, ob A oder B gewählt wird.
- Wenn Sie Geld vom Konto A auf Konto B überweisen, wird zuerst A
  belastet und anschliessend der Betrag B gutgeschrieben.
- Was passiert, wenn die Transaktion in der Mitte abbricht?

Systemmodell
````````````

- Prozesse pi (i=1,2,...N).
- Kommunikation mithilfe von Nachrichtenaustausch.
    - Nachrichten können signiert oder unsigniert sein.
- Zufällige („byzantinische“) Prozessausfälle sind möglich.
    - Beispielsweise können f Prozesse fehlerhaft sein.

Definition des Konsens-Problems
```````````````````````````````

- Prozesse pi befinden sich in einem nicht-dedizierten Zustand zi
    - Jeder Prozess schlägt einen Wert vi aus D vor (i=1,...N)
- Der Prozess wechselt in den dediziertern Zustand zj
    - Jeder Prozess setzt den Wert einer Entscheidungsvariable di.
    - di ist fix und darf nicht verändert werden.

Konsens-Bedingungen
- Terminierung: jeder Prozess setzt irgendwann seine
  Entscheidungsvariable.
- Einigung: der Entscheidungswert aller korrekten Prozesse ist gleich:
  di=dj falls pi und pj korrekt sind und in den dedizierten Zustand
  übergegangen sind.
- Integrität: wenn alle korrekten Prozesse denselben Wert
  vorgeschlagen haben, dann hat jeder pi im dedizierten Zustand
  diesen Wert gewählt.

Konsensfindung:
- Geschieht beispielsweise durch majority-Voting, indem jeder
  Prozess seine Entscheidung jedem andern Prozess mitteilt und
  dann einen Mehrheitsentscheid akzeptiert.

.. figure:: images/zugz/wuk_konsens_ex1.png

   Ergebnis: p1 und p2 einigen sich nach dem Absturz von p3 auf *„proceed“*


Byzantinische Generäle
``````````````````````

**Problemstellung (Lamport)**:
- Drei oder mehr (byzantinische) Generäle müssen sich darauf einigen
  anzugreifen oder sich zurück zu ziehen.
- Einer der Generäle gibt die Befehle / den Auftrag;
  die andern melden den Auftrag weiter und führen aus.
- Einer oder mehrere der Generäle handeln „betrügerisch“ (bzw. melden
  falsche Bestände):
    - Es wird ein falscher Befehl weitergegeben.
        - Falls der Befehl „Angriff“ lautet wird „Rückzug“ weitergemeldet
          oder umgekehrt.

**Forderungen**:

- Terminierung:
    - Irgendwann setzt jeder korrekte Prozess seine
      Entscheidungsvariable.
- Einigung:
    - Der Entscheidungswert aller korrekten Prozesse ist gleich (falls pi
      und pj korrekt sind, dann gilt di=d j).
- Integrität:
    - Wenn der Befehlshaber korrekt ist, einigen sich alle korrekten
      Prozesse auf den von ihm vorgeschlagenen Wert.

**Allgemeine Formulierung**:

- Annahmen:
    - n Generäle, m davon sind „Verräter“ (verursachen byzantinische
      Fehler).
    - Jeder General Gi hat den initialen Wert xi und möchte zusammen mit
      den andern zu einem finalen Entscheid di gelangen.
- Annahmen betreffend Kommunikation:
    - synchron [synchrone Uhren]
    - authentifiziert [Empfänger kennt Identität des Absenders]
    - Punkt-zu-Punkt [jeder ist mit jedem direkt verbunden]

**Lösung**:

*Lösung nach Lamport (1982)*

- Jeder General teilt allen andern sein Wissen mit
  (Truppenstärke und evtl. weitere Informationen wie „Angriff /
  „Rückzug“).
    - Die Guten sagen die Wahrheit.
    - Die Bösen sagen irgend etwas.
- Jeder General teilt allen andern seine Ergebnisse aus der vorherigen
  Runde mit.
- Jeder General nimmt als i-tes Element seines Entscheidungsvektors
  die Mehrheit der gelesenen Werte.
    - Falls keine Mehrheit bestimmt werden kann, dann wird der Wert auf
    „unbekannt“ gesetzt.


.. figure:: images/zugz/wuk_konsens_byzant_1.png

   Im Falle von drei Prozessen ist ein Entscheid nicht möglich

   allgemein im Falle N<=3f

   Im Diagramm links:

   - p1 sendet an beide Untergebene die Nachricht v (1:v).
   - p2 sendet an p3 diese Nachricht weiter (2:1:v).
   - p3 sendet an p2 die falsche Nachricht (3:1:u).
   - p2 kann nicht entscheiden.

.. figure:: images/zugz/wuk_konsens_byzant_2.png

   Vier Generäle

   p2 beschliesst: majority(v,u,v)=v
   p4 beschliesst: majority(v,v,w)=v


Die byzantinischen Systeme setzen synchrone Kommunikation
voraus:

- Der Nachrichtenaustausch geschieht in einzelnen Runden.

**Fisher et al haben 1985 gezeigt, dass es in asynchronen Systemen**
**nicht möglich ist, Konsens zu garantieren.**

Grund:

- Sie können nicht unterscheiden, ob in einem asynchronen System eine
  Nachricht überhaupt nicht eintrifft oder sehr verspätet.
    - Damit ist eine (schnelle) Entscheidungsfindung nicht mehr möglich.

Für **asynchrone Systeme** gibt es

PAXOS
`````

Wie kann man Konsens und Datenkonsistenz in verteilten Systemen
erreichen, obschon unterschiedlichste Fehler im System auftreten?

Konsens in verteilten Systemen betrifft konkret einen Wert einer
Grösse, welche von allen Beteiligten des Systems als akzeptierter
Wert angesehen wird.

PAXOS ist eine Familie von Protokollen, welche das Konsens
Problem in unzuverlässigen verteilten Systemen löst.
- Konsens:
    - Übereinstimmung betreffend einer Systemgrösse durch die
      beteiligten Prozesse.
- Leslie Lamport:
    - “The Paxos algorithm, when presented in plain English, is very
      simple”

**Voraussetzungen**:

- Safety
    - Es kann nur ein Wert ausgewählt werden, welcher vorgeschlagen
      wurde.
    - Es kann nur genau ein Wert ausgewählt werden.
    - Ein Knoten / Systemteilnehmer weiss erst dann, dass ein bestimmter
      Wert gewählt wurde, wenn dies auch wirklich geschehen ist.
- Liveness
    - Ein vorgeschlagener Wert wird eventuell ausgewählt.
    - Falls ein Wert ausgwählt wurde, kann ein Knoten diesen Wert als den
      ausgewählten Wert erkennen.
- Der Basic PAXOS Algorithmus setzt voraus, dass nicht-
  Byzantinische Fehler auftreten können.
- Diese Fehler könnten zu einer der folgenden Kategorien gehören:
    1. Fail-Stops oder Crashes, welche spontan auftreten können.
    2. Omission oder Partielle Diskontinuation:
       Nachrichten könnten verloren gehen, aber sie sind alle korrekt.
    3. Es könnten Timing Fehler auftreten:
        - Nachrichten könnten nicht in der korrekten Reihenfolge oder doppelt
          angeliefert werden.
- Es gibt unterschiedliche PAXOS Algorithmen:
    - Basic
    - Zusätzlich mit Berücksichtigung byzantinischer Fehler.

Paxo's Notation
```````````````

Agenten-Klassen:
- Das Modell enthält die folgenden drei Agenten-Rollen:
    - Proposers
    - Acceptors
    - Learners
- Die Rollen können alle mehrfach besetzt sein.
- Ein Knoten/Prozessor kann mehr als eine Client Rolle haben, also
  gleichzeitig Proposer und Acceptor .... sein.

.. figure:: images/zugz/wuk_wahl_paxos_example.png

    Beispiel: System mit einem Acceptor

    Falls der Proposer dem Acceptor einen Wert vorschlägt, akzeptiert der
    Acceptor diesen Wert und dann kann der Learner lernen, dass dieser
    Wert im System akzeptiert wurde.

    Problem dieses Beispielsystems:
    - Sobald der Acceptor ausfällt ist Schluss.

.. figure:: images/zugz/wuk_wahl_paxos_example2.png

    Beispiel: System mit mehreren Acceptoren (Quorum)

   - Ein Proposal wird von mehreren Acceptoren akzeptiert.
   - Jeder Acceptor kann genau einen Wert akzeptieren.
   - Ein Quorum ist ein minimales Set von Acceptoren, welche fehlerfrei
     arbeiten.
   - Ein Wert gilt als akzeptiert sobald eine Mehrheit diesen Wert als den
     richtigen akzeptiert hat.


.. figure:: images/zugz/wuk_wahl_paxos_example2.png

    Basic PAXOS Modell: mehrere Proposer und Acceptors
    
     Idee:
    - Sobald ein Acceptor Quorum einen Vorschlag angenommen hat, legt
      das System diesen Wert fest und alle Beteiligten können diesen
      Wert "lernen".
        - Wie dieser Zustand erreicht wird bleibt unklar:
            - Im schlimmsten Fall könnten mehrere Proposers
              unterschiedliche Werte "gleichzeitig" vorschlagen.
              Welcher Wert sol dann genommen werden?
            - Es würde kein Konsens bestehen, das System wäre blockiert.
    - Ausweg:
        - Acceptors können mehrere Werte empfangen (und dann den
          höchsten auswählen).


**Anforderungen an das System**

- Nontriviality:
    - Es kann lediglich genau ein vorgeschlagener Wert ausgewählt
      werden.
- Stability:
    - Es wird lediglich ein Wert ausgewählt.
- Consistency:
    - Zwei Lerner können nicht unterschiedlche Werte lernen,

Paxos Algorihmus
````````````````

**Phase 1 (prepare)**

- Ein Proposer selektiert eine Proposal Nummer n und sendet einen
  "Prepare Request" mit der Nummer n an mindestens ein Acceptor
  Quorum.
- Falls ein Acceptor einen "Prepare Request" mit einer Nummer n erhält,
  mit n > als alle bisherigen Requests,
  dann antwortet er mit "YES"
  und verspricht, keinen weiteren Wert mehr < n zu akzeptieren.
- Falls er bereits einem höheren n' zugestimmt hat,
  sendet er eine NACK "non-acknowledge" Nachricht.
- Die Antwort-Nachricht enthält den höchsten akzeptierten Wert (n und
  den dazugehörenden Wert v).

**Phase 2 (accept)**

- Sobald der Proposer von den meisten Acceptoren ein "YES" als
  Antwort bekommen hat,
  sendet der Proposer eine "Accept Request" Nachricht an alle
  Acceptors zusammen mit <n,v> (höchster akzeptiertes n und
  zueghörender Wert v).
- Der Acceptor muss diesen Wert akzeptieren,
  sofern er nicht einem höheren n bereits zugestimmt hat.

.. figure:: images/zugz/wuk_wahl_paxos_flow1.png

   Basic PAXOS message flow (erfolgreich)

.. figure:: images/zugz/wuk_wahl_paxos_flow2.png

   Ausfall Proposer


Anwendungen
...........

**Anwenungsbeispiele:**

- In verteilten Datenbanken wird der PAXOS Algorithmus eingesetzt,
um konsistente Daten zu erhalten und zu behalten.
- In virtuellen Disk-Systemen, welche verteilt sind, dient der PAXOS
Algorithmus für die Konsistenzhaltung der Datenbestände.

