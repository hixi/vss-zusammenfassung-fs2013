Code Schnipsel
==============

.. _sockets_src:

Sockets/Server
--------------

Multithreaded Server
....................

**non-blocking threaded server**::

    public class TCPServer {
        public static void main(String args[]) {
            try {
                int serverPort = 7896;
                System.out.println("Server gestartet!");
                ServerSocket listenSocket = new ServerSocket(serverPort);
                while (true) {
                    Socket clientSocket = listenSocket.accept();
                    System.out.println("Neue Verbindung! Erzeuge TCPConnection...");
                    Connection c = new Connection(clientSocket);
                    c.start();
                }
            } catch(IOException e) {
                System.out.println("Listen: " + e.getMessage());
            }
        }
    }

    class TCPConnection extends Thread {
        DataInputStream in;
        DataOutputStream out;
        Socket clientSocket;
        public TCPConnection(Socket aClientSocket) {
            try {
                clientSocket = aClientSocket;
                in = new DataInputStream(clientSocket.getInputStream());
                out =
                    new DataOutputStream(
                        clientSocket.getOutputStream());
                this.run();
            } catch(IOException e) {
                System.out.println("Connection: " + e.getMessage());
            }
        }
        public void run() {
            try {
                String data = in.readUTF();
                out.writeUTF(data+" " + InetAddress.getLocalHost());
                clientSocket.close();
            } catch (EOFException e) {
                System.out.println("EOF:" + e.getMessage();
            } catch (IOException e) {
                System.out.println("IO:" + e.getMessage();
            }
        }
    }


Client
......

**blocking client**::

    public class TCPClient {
        public static void main(String args[]) {
            try {
                Socket s;
                String host, msg;
                int serverPort = 7896;
                if (args.length<1) {
                    host="localhost";
                    msg="Hello World";
                } else {
                    host=args[0];
                    msg=args[1];
                }
                s = new Socket(host, serverPort);
                DataInputStream in = new DataInputStream(s.getInputStream());
                DataOutputStream in = new DataOutputStream(s.getOutputStream());
                out.writeUTF(msg);
                String data = in.readUTF();
                System.out.println("Received:" + data);
                s.close();
            } catch (UnknownHostException e) {
                System.out.println("Socket: " + e.getMessage());
            } catch (EOFException e) {
                System.out.println("EOF: " + e.getMessage());
            } catch (IOException e) {
                System.out.println("IO: " + e.getMessage());
            }
        }
    }



**non-blocking client**::

    public class Socket_NonBlocking {
        public static void main(String args[]) {
            try {
                InetAddress addr = InetAddress.getByName("localhost");
                int port = 9090;
                SocketAddress sockaddr = new InetSocketAddress(addr,port);
                Socket sock = new Socket();
                int timeoutMs = 2000;
                sock.connect(sockaddr, timeoutMs);
            } catch (UnknownHostException e) {
            } catch (SocketTimeoutException e) {
            } catch (IOException e) {
            }
        }
    }



.. _messaging_src:

Messaging
---------

**Einfacher Queue Sender**::

    Connection connection;
    connection = connectionFactory.createConnection();
    connection.start();
    Session session = connection.createSession(mp.transacted,
        Session.AUTO_ACKNOWLEDGE);
    Destination destination = session.createQueue("testQueue");
    MessageProducer producer = session.createProducer(destination);
    producer.setDeliveryMode(DeliveryMode.PERSISTENT);
    TextMessage message = session.createTextMessage("hello");
    producer.send(message);
    producer.close();
    session.close();
    connection.close();


**Einfacher Queue Receiver**::

    Connection connection;
    connection = connectionFactory.createConnection();
    connection.start();
    Session session =
    connection.createSession(mc.transacted,
    Session.AUTO_ACKNOWLEDGE);
    Destination destination =
    session.createQueue("testQueue");
    MessageConsumer consumer =
    session.createConsumer(destination);
    TextMessage text=(TextMessage) consumer.receive();
    consumer.close();
    session.close();
    connection.close();


JMS - Publish/Subscribe
.......................

**Publisher**::

    public void run() {
        try {
            HelloWorldPublisher mp = new HelloWorldPublisher();
            ActiveMQConnectionFactory connectionFactory = new
            ActiveMQConnectionFactory(mp.user, mp.password, mp.url);
            Connection connection = connectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(mp.transacted,
            Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createTopic(mp.topicName);
            MessageProducer producer = session.createProducer(destination);
            TextMessage message =
            session.createTextMessage(mp.messageText);
            producer.send(message);
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


**Subscriber**::

    public static void main(String[] args) {
        try {
            HelloSubscriber hs = new HelloSubscriber();
            ActiveMQConnectionFactory connectionFactory = new
            ActiveMQConnectionFactory(hs.user, hs.password, hs.url);
            Connection connection =
            connectionFactory.createConnection();
            Session session = connection.createSession(hs.transacted,
            Session.AUTO_ACKNOWLEDGE);
            Destination destination=session.createTopic(hs.topicName);
            MessageConsumer consumer=
            session.createConsumer(destination);
            connection.start();
            TextMessage text = (TextMessage) consumer.receive();
        } catch (Exception e1) {e1.printStackTrace();}
    }


JavaScpaces
...........

**HelloWorld**::

    public class HelloWorld {
        public static void main(String[] args) {
            try {
                    Message msg = new Message();
                    msg.content = "Hello World";
                    JavaSpace space = SpaceAccessor.getSpace();
                    space.write(msg, null, Lease.FOREVER);
                    Message template = new Message();
                    Message result = (Message)space.read(template,null,Long.MAX_VALUE);
                    System.out.println(result.content);
                } catch (Exception e) {
                    e.printStackTrace();
            }
        }
    }





.. _hadoop_src:

Hadoop
------

**WordCount**::

    import java.io.IOException;
    import java.util.Iterator;
    import java.util.StringTokenizer;

    import org.apache.hadoop.fs.Path;
    import org.apache.hadoop.io.IntWritable;
    import org.apache.hadoop.io.LongWritable;
    import org.apache.hadoop.io.Text;
    import org.apache.hadoop.mapred.FileInputFormat;
    import org.apache.hadoop.mapred.FileOutputFormat;
    import org.apache.hadoop.mapred.JobClient;
    import org.apache.hadoop.mapred.JobConf;
    import org.apache.hadoop.mapred.MapReduceBase;
    import org.apache.hadoop.mapred.Mapper;
    import org.apache.hadoop.mapred.OutputCollector;
    import org.apache.hadoop.mapred.Reducer;
    import org.apache.hadoop.mapred.Reporter;
    import org.apache.hadoop.mapred.TextInputFormat;
    import org.apache.hadoop.mapred.TextOutputFormat;

    public class WordCount {

      public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();

        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
          String line = value.toString();
          StringTokenizer tokenizer = new StringTokenizer(line);
          while (tokenizer.hasMoreTokens()) {
            word.set(tokenizer.nextToken());
            output.collect(word, one);
          }
        }
      }

      public static class Reduce extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {
        public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
          int sum = 0;
          while (values.hasNext()) {
            sum += values.next().get();
          }
          output.collect(key, new IntWritable(sum));
        }
      }

      public static void main(String[] args) throws Exception {
        JobConf conf = new JobConf(WordCount.class);
        conf.setJobName("wordcount");

        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(IntWritable.class);

        conf.setMapperClass(Map.class);
        conf.setCombinerClass(Reduce.class);
        conf.setReducerClass(Reduce.class);

        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);

        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));

        JobClient.runJob(conf);
      }
    }

**MyMapReduce**::

    package uebung_05_1_Lsg;

    import java.io.BufferedWriter;
    import java.io.IOException;
    import java.nio.charset.Charset;
    import java.nio.file.DirectoryStream;
    import java.nio.file.Files;
    import java.nio.file.Path;
    import java.nio.file.Paths;
    import java.nio.file.StandardOpenOption;
    import java.util.ArrayList;
    import java.util.Arrays;
    import java.util.List;
    import java.util.StringTokenizer;

    public class MyMapReduce {

        public static void main(String[] args) {
            MyMapReduce mr = new MyMapReduce();
            String input = "C:/Temp/Input";
            // Work könnte auch temporäres Verzeichnis sein...
            String temp = "C:/Temp/Work";
            String output = "C:/Temp/Output";

            /*
             * Eingabe: diverse Text Dateien *.txt Ausgabe: diverse Text Dateien
             * *.txt im Work Verzeichnis
             */
            mr.map(input, temp);
            /*
             * Eingabe: diverse Text Dateien *.txt im Work Verzeichnis Ausgabe:
             * diverse sortierte Text Dateien *.sorted im Work Verzeichnis
             */
            mr.shuffle(temp);
            mr.reduceBlock(temp);
            mr.reduce(temp, output);

        }

        public void map(String inDir, String outDir) {
            System.out.println();
            System.out.println(">>> map(" + inDir + " , " + outDir + ")");
            Path path = Paths.get(inDir);
            try (DirectoryStream<Path> ds = Files.newDirectoryStream(path)) {
                for (Path file : ds) {
                    System.out.println(file.getFileName());
                    Charset charset1 = Charset.forName("ISO-8859-1");
                    Charset charset2 = Charset.forName("UTF-8");

                    Path file_path;
                    file_path = Paths.get(outDir, file.getFileName().toString());
                    BufferedWriter writer = Files.newBufferedWriter(file_path,
                            charset2, StandardOpenOption.CREATE);
                    List<String> lines = Files.readAllLines(file, charset1);
                    for (String line : lines) {
                        // System.out.println(line);
                        StringTokenizer tokenizer = new StringTokenizer(line,
                                ".,;: !\n\r");
                        while (tokenizer.hasMoreTokens()) {
                            String token = tokenizer.nextToken();
                            writer.write(token + ",1");
                            writer.newLine();
                            // System.out.println("[TOKEN]" + token);
                        }
                    }
                    writer.flush();
                    writer.close();

                }
            } catch (IOException e) {
                System.err.println(e);
            }
        }

        public void shuffle(String workDir) {
            System.out.println();
            System.out.println(">>> shuffle(" + workDir + ")");
            boolean debug = true;
            Path path = Paths.get(workDir);
            try (DirectoryStream<Path> ds = Files.newDirectoryStream(path, "*.txt")) {
                for (Path file : ds) {
                    System.out.println(file.getFileName());
                    Charset charset1 = Charset.forName("UTF-8");// "ISO-8859-1");

                    Path file_path;
                    if (debug) {
                        /*
                         * Dateien / Buffer stehen lassen
                         */
                        file_path = Paths.get(workDir, file.getFileName()
                                .toString() + ".sorted");
                    } else {
                        /*
                         * Buffer existieren nur temporär
                         */
                        String temp_prefix = "Buffer_";
                        String temp_sufix = ".sorted";
                        file_path = Files.createTempFile(temp_prefix, temp_sufix);
                    }
                    Charset charset2 = Charset.forName("UTF-8");
                    BufferedWriter writer = Files.newBufferedWriter(file_path,
                            charset2, StandardOpenOption.CREATE);
                    List<String> lines = Files.readAllLines(file, charset1);
                    Object[] oArray = lines.toArray();
                    // for (Object o : oArray)
                    // System.out.println("[ObjectArray]" + o);
                    Arrays.sort(oArray);
                    for (Object token : oArray) {
                        writer.write(token.toString());
                        writer.newLine();
                        // System.out.println(token);
                    }
                    writer.flush();
                    writer.close();

                }
            } catch (IOException e) {
                System.err.println(e);
            }

        }

        public void reduceBlock(String inDir) {
            System.out.println();
            System.out.println(">>> reduceBlock(" + inDir + ")");

            Path path = Paths.get(inDir);
            try (DirectoryStream<Path> ds = Files.newDirectoryStream(path,
                    "*.sorted")) {

                List<Path> pathList = new ArrayList<Path>();
                for (Path file : ds) {
                    Path file_path = Paths
                            .get(inDir, file.getFileName().toString());
                    System.out.println(file.getFileName());
                    pathList.add(file_path);
                }

                Charset charset = Charset.forName("UTF-8");
                for (int i = 0; i < pathList.size(); i++) {
                    List<String> lines = Files.readAllLines(pathList.get(i),
                            charset);
                    // System.out.println(lines.toString());
                    Path file_path = Paths.get(inDir, pathList.get(i).getFileName()
                            .toString()
                            + ".reduced");
                    BufferedWriter writer = Files.newBufferedWriter(file_path,
                            charset, StandardOpenOption.CREATE);

                    String[] items = new String[lines.size()];
                    for (int j = 0; j < lines.size(); j++) {
                        items[j] = lines.get(j);
                    }

                    for (int k = 1; k <= lines.size(); k++) {
                        int eq = 1;
                        while ((k < lines.size())
                                && (items[k - 1].equals(items[k]))) {
                            eq++;
                            k++;
                        }
                        // k-1: for geht bis k
                        String[] parts = items[k - 1].split(",");
                        String newItem = parts[0] + "," + eq;
                        // System.out.println("[NewItem]" + newItem);

                        writer.write(newItem);
                        writer.newLine();
                        // System.out.println("[TOKEN]" + token);
                    }
                    writer.flush();
                    writer.close();
                }

            } catch (IOException e) {
                System.err.println(e);
            }

        }

        public void reduce(String workDir, String outDir) {
            System.out.println();
            System.out.println(">>> reduce(" + workDir + " , " + outDir + ")");

            Path path = Paths.get(workDir);
            try (DirectoryStream<Path> ds = Files.newDirectoryStream(path,
                    "*.reduced")) {

                List<Path> pathList = new ArrayList<Path>();
                for (Path file : ds) {
                    Path file_path = Paths.get(workDir, file.getFileName()
                            .toString());
                    System.out.println(file_path);
                    pathList.add(file_path);
                }

                Charset charset = Charset.forName("UTF-8");
                int nrFiles = pathList.size();
                /*
                 * Anzahl Dateien: nrFiles EInlesen aller Tokens und Speichern der
                 * Tokens in einem einzigen Array (alle)
                 */

                String[][] dd = new String[nrFiles][];
                int total = 0;
                // das ginge auch einfacher!!
                for (int i = 0; i < nrFiles; i++) {
                    List<String> lines = Files.readAllLines(pathList.get(i),
                            charset);
                    int nrLines = lines.size();
                    dd[i] = new String[nrLines];
                    total = total + nrLines;
                    for (int h = 0; h < nrLines; h++) {
                        dd[i][h] = lines.get(h);
                    }
                    // System.out.println(lines.toString());
                }

                /*
                 * sortieren des Arrays mit allen Tokens
                 */
                String[] alle = new String[total];
                int iLines = 0;
                for (int e = 0; e < nrFiles; e++) {
                    for (int f = 0; f < dd[e].length; f++) {
                        alle[iLines] = dd[e][f];
                        iLines++;
                    }
                }
                Arrays.sort(alle);

                // for (int p = 0; p < total; p++)
                // System.out.print(alle[p] + " ");
                // System.out.println();

                Path file_path = Paths.get(outDir, "Total.reduced");
                BufferedWriter writer = Files.newBufferedWriter(file_path, charset,
                        StandardOpenOption.CREATE);

                String[] parts = alle[0].split(",");
                String[] partsm;
                int eq = 0;
                String newItem;
                for (int k = 1; k < total; k++) {
                    // System.out.println(alle[k - 1]);
                    partsm = parts;
                    parts = alle[k].split(",");
                    eq = Integer.parseInt(partsm[1]);
                    while ((k < total) && (parts[0].equals(partsm[0]))) {
                        eq = eq + Integer.parseInt(partsm[1]);
                        k++;
                        parts = alle[k].split(",");
                    }

                    newItem = partsm[0] + "," + eq;
                    // System.out.println("[NewItem]" + newItem);

                    writer.write(newItem);
                    writer.newLine();
                }
                // Ausgabe des letzten Tokens (oben ist ja partsm)
                newItem = parts[0] + "," + eq;
                // System.out.println(">>> last: "+ newItem);
                writer.write(newItem);
                writer.newLine();

                writer.flush();
                writer.close();

            } catch (IOException e) {
                System.err.println(e);
            }

        }

    }


**TreeMapWordCounter**::

    package uebung_05_1_Lsg;

    import java.io.FileNotFoundException;

    import java.io.FileReader;
    import java.util.Scanner;
    import java.util.TreeMap;

    /*
     * Einfache Variante: liest nur eine Datei und
     * verwendet die Scanner Klasse, um jeweils das
     * nächste Wort zu lesen (mittels Scanner Iterator)
     *
     * TreeMap ist O(n*lg(n))
     *
     */
    public class TreeMapWordCounter {
        public static void main(String[] args) {
            TreeMap<String, Integer> frequencyData = new TreeMap<String, Integer>();

            readWordFile(frequencyData);
            printAllCounts(frequencyData);
        }

        public static int getCount(String word,
                TreeMap<String, Integer> frequencyData) {
            if (frequencyData.containsKey(word)) {
                // Wort ist bereits im Baum / in der Map,
                // => lesen des Integer (Objekt)
                return frequencyData.get(word); // Auto-unboxed
            } else {
                // Wort ist neu: also Anzahl = 0
                return 0;
            }
        }

        public static void printAllCounts(TreeMap<String, Integer> frequencyData) {
            System.out.println("-----------------------------------------------");
            System.out.println("    Anzahl        Wort");
            System.out.println("-----------------------------------------------");

            // Der Key Iterator ist aufsteigend sortiert
            for (String word : frequencyData.keySet()) {
                // Integer frequencyData.get(key) key= word
                System.out.printf("%15d    %s\n", frequencyData.get(word), word);
            }

            System.out.println("-----------------------------------------------");
        }

        public static void readWordFile(TreeMap<String, Integer> frequencyData) {
            Scanner wordFile;	// Scanner Iterator:
            // Object java.util.Scanner implements Closeable, AutoCloseable, Iterator<String>
            String word; 		// nächstes Wort in der Eingabedatei
            Integer count; 		// Anzahl des Worts im Eingabetext

            try {
                wordFile = new Scanner(new FileReader("words.txt"));
            } catch (FileNotFoundException e) {
                System.err.println(e);
                return;
            }

            while (wordFile.hasNext()) {
                // Iterator: lies nächstes Wort
                // Evtl muss EOL (\n oder \r) abgeschnitten werden.
                word = wordFile.next();

                // Lesen des bisherigen Werts / Anzahl
                // +1 liefert so oder so den neuen Wert
                count = getCount(word, frequencyData) + 1;
                frequencyData.put(word, count);
            }
            wordFile.close();
        }

    }


**MyThreadedTreeMapCallable**::

    package uebung_05_1_Lsg;

    import java.io.BufferedReader;
    import java.io.BufferedWriter;
    import java.io.FileNotFoundException;
    import java.io.IOException;
    import java.nio.charset.Charset;
    import java.nio.file.DirectoryStream;
    import java.nio.file.Files;
    import java.nio.file.Path;
    import java.nio.file.Paths;
    import java.nio.file.StandardOpenOption;
    import java.util.ArrayList;
    import java.util.Collection;
    import java.util.List;
    import java.util.StringTokenizer;
    import java.util.TreeMap;
    import java.util.concurrent.Callable;
    import java.util.concurrent.ExecutionException;
    import java.util.concurrent.ExecutorService;
    import java.util.concurrent.Executors;
    import java.util.concurrent.Future;

    public class MyThreadedMapCallable {

        public static void main(String[] args) {
            String input = "C:/Temp/Input";
            String outDir = "C:/Temp/Output/Result.txt";
            try {
                MyThreadedMapCallable.Map.map(input, outDir);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        public static class Map {

            public static void map(String inDir, String outDir) throws IOException {
                ExecutorService executor = Executors.newCachedThreadPool();
                Path inPath = Paths.get(inDir);
                Path outPath = Paths.get(outDir);
                try (DirectoryStream<Path> ds = Files.newDirectoryStream(inPath)) {
                    List<Future<TreeMap<String, Integer>>> trees = new ArrayList<Future<TreeMap<String, Integer>>>();
                    Collection<Worker> workers = new ArrayList<Worker>();
                    for (Path file : ds) {
                        System.out.println("[EingabeDatei]" + file.getFileName());
                        Worker worker = new Worker(file);
                        workers.add(worker);
                    }
                    // trees enthält die TreeMaps
                    // je eine pro Eingabedatei
                    trees = executor.invokeAll(workers);
                    executor.shutdown();
                    // der Reducer muss die Bäume zusammenfassen
                    Reducer reducer = new Reducer(trees, outPath);
                    reducer.reduceAll();

                } catch (IOException io) {
                    System.err.println(io.getMessage());
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }

        public static class Worker implements Callable<TreeMap<String, Integer>> {
            private TreeMap<String, Integer> frequencyData = new TreeMap<String, Integer>();
            private Path inFile;

            Worker() {
            }

            Worker(Path file) {
                inFile = file;
            }

            public static int getCount(String word,
                    TreeMap<String, Integer> frequencyData) {
                if (frequencyData.containsKey(word)) {
                    // Wort ist bereits im Baum / in der Map,
                    // => lesen des Integer (Objekt)
                    return frequencyData.get(word); // Auto-unboxed
                } else {
                    // Wort ist neu: also Anzahl = 0
                    return 0;
                }
            }

            private void readWordFile(Path in) {
                BufferedReader wordFile;
                String line;

                String word; // nächstes Wort in der Eingabedatei
                Integer count; // Anzahl des Worts im Eingabetext

                try {
                    wordFile = Files.newBufferedReader(in,
                            Charset.forName("ISO-8859-1"));
                    line = wordFile.readLine();
                    while (line != null) {
                        StringTokenizer tokenizer = new StringTokenizer(line,
                                ".,;: !\n\r");
                        while (tokenizer.hasMoreTokens()) {
                            word = tokenizer.nextToken();
                            // Lesen des bisherigen Werts / Anzahl
                            // +1 liefert so oder so den neuen Wert
                            count = getCount(word, frequencyData) + 1;
                            frequencyData.put(word, count);
                        }
                        line = wordFile.readLine();
                    }
                    wordFile.close();

                } catch (FileNotFoundException e) {
                    System.err.println("[File not found: " + e.getMessage());
                    return;
                } catch (IOException e) {
                    System.err.println("[IOException: " + e.getMessage());
                    e.printStackTrace();
                    return;
                }
            }

            public TreeMap<String, Integer> getFrequencyData() {
                return frequencyData;
            }

            public static void printAllCounts(TreeMap<String, Integer> frequencyData) {
                for (String word : frequencyData.keySet()) {
                    // Integer frequencyData.get(key) key= word
                    System.out
                            .printf("%15d    %s\n", frequencyData.get(word), word);
                }
            }

            // for single threaded (only one input file)
            public void run() {
                Worker worker = new Worker();
                worker.readWordFile(inFile);
                Worker.printAllCounts(worker.getFrequencyData());
            }

            // Executer call method
            public TreeMap<String, Integer> call() throws Exception {
                Worker worker = new Worker();
                worker.readWordFile(inFile);
                return worker.getFrequencyData();
            }
        }

        public static class Reducer {
            private TreeMap<String, Integer> frequencyTotal = new TreeMap<String, Integer>();
            private TreeMap<String, Integer> tempData = null;
            List<Future<TreeMap<String, Integer>>> futureTreeList;

            private Path outDir;

            Reducer() {
            }

            Reducer(TreeMap<String, Integer> tree, Path out) {
                tempData = tree;
                outDir = out;
            }

            Reducer(List<Future<TreeMap<String, Integer>>> futureList, Path out) {
                futureTreeList = futureList;
                outDir = out;
            }

            public void reduceAll() {
                frequencyTotal = reduceList();
                printAllCounts(frequencyTotal);
                writeWordFile(outDir);
            }

            public void reduce() {
                int count = 0;
                for (String key : tempData.keySet()) {
                    if (frequencyTotal.containsKey(key)) {
                        count = frequencyTotal.get(key);
                    } else {
                        count = 0;
                    }
                    frequencyTotal.put(key, count + tempData.get(key));
                }
                // Worker.printAllCounts(frequencyTotal);
                writeWordFile(outDir);
            }

            public TreeMap<String, Integer> reduceList() {
                TreeMap<String, Integer> result = new TreeMap<String, Integer>();
                for (Future<TreeMap<String, Integer>> future : futureTreeList) {
                    TreeMap<String, Integer> treeMap;
                    try {
                        treeMap = future.get();
                        for (String key : treeMap.keySet()) {
                            int count = 0;
                            if (result.containsKey(key)) {
                                // Wort ist bereits im Baum / in der Map,
                                // => lesen des Integer (Objekt)
                                count = result.get(key); // Auto-unboxed
                            } else {
                                // Wort ist neu: also Anzahl = 0
                                count = 0;
                            }
                            count = count + treeMap.get(key);
                            result.put(key, count);
                        }
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }
                return result;
            }

            private void writeWordFile(Path out) {
                Path outDir = out;
                BufferedWriter wordFile;

                try {
                    wordFile = Files.newBufferedWriter(outDir,
                            Charset.forName("UTF-8"), StandardOpenOption.CREATE);

                    for (String key : frequencyTotal.keySet()) {
                        wordFile.write(key + "," + frequencyTotal.get(key));
                        wordFile.newLine();
                    }
                    wordFile.close();

                } catch (FileNotFoundException e) {
                    System.err.println("[File not found: " + e.getMessage());
                    return;
                } catch (IOException e) {
                    System.err.println("[IOException: " + e.getMessage());
                    e.printStackTrace();
                    return;
                }
            }

            public static void printAllCounts(TreeMap<String, Integer> frequencyData) {
                for (String word : frequencyData.keySet()) {
                    // Integer frequencyData.get(key) key= word
                    System.out
                            .printf("%15d    %s\n", frequencyData.get(word), word);
                }
            }
        }

    }

.. _dirty_simple_python_map_reduce_src:

Dirty Simple Map Reduce in Python
---------------------------------

.. code-block:: python

    from collections import namedtuple
    from math import fsum

    def map_reduce(data, mapper, reducer=None):
        '''Simple map/reduce for data analysis.

        Each data element is passed to a *mapper* function.
        The mapper returns key/value pairs
        or None for data elements to be skipped.

        Returns a dict with the data grouped into lists.
        If a *reducer* is specified, it aggregates each list.

        >>> def even_odd(elem):                     # sample mapper
        ...     if 10 <= elem <= 20:                # skip elems outside the range
        ...         key = elem % 2                  # group into evens and odds
        ...         return key, elem

        >>> map_reduce(range(30), even_odd)         # show group members
        {0: [10, 12, 14, 16, 18, 20], 1: [11, 13, 15, 17, 19]}

        >>> map_reduce(range(30), even_odd, sum)    # sum each group
        {0: 90, 1: 75}

        '''
        d = {}
        for elem in data:
            r = mapper(elem)
            if r is not None:
                key, value = r
                if key in d:
                    d[key].append(value)
                else:
                    d[key] = [value]
        if reducer is not None:
            for key, group in d.items():
                d[key] = reducer(group)
        return d

    Summary = namedtuple('Summary', ['n', 'lo', 'mean', 'hi', 'std_dev'])

    def describe(data):
        'Simple reducer for descriptive statistics'
        n = len(data)
        lo = min(data)
        hi = max(data)
        mean = fsum(data) / n
        std_dev = (fsum((x - mean) ** 2 for x in data) / n) ** 0.5
        return Summary(n, lo, mean, hi, std_dev)


    if __name__ == '__main__':

        from pprint import pprint
        import doctest

        Person = namedtuple('Person', ['name', 'gender', 'age', 'height'])

        persons = [
            Person('mary', 'fem', 21, 60.2),
            Person('suzy', 'fem', 32, 70.1),
            Person('jane', 'fem', 27, 58.1),
            Person('jill', 'fem', 24, 69.1),
            Person('bess', 'fem', 43, 66.6),
            Person('john', 'mal', 25, 70.8),
            Person('jack', 'mal', 40, 59.1),
            Person('mike', 'mal', 42, 60.3),
            Person('zack', 'mal', 45, 63.7),
            Person('alma', 'fem', 34, 67.0),
            Person('bill', 'mal', 20, 62.1),
        ]

        def height_by_gender_and_agegroup(p):
            key = p.gender, p.age //10
            val = p.height
            return key, val

        pprint(persons)                                                      # upgrouped dataset
        pprint(map_reduce(persons, lambda p: ((p.gender, p.age//10), p)))    # grouped people
        pprint(map_reduce(persons, height_by_gender_and_agegroup, None))     # grouped heights
        pprint(map_reduce(persons, height_by_gender_and_agegroup, len))      # size of each group
        pprint(map_reduce(persons, height_by_gender_and_agegroup, describe)) # describe each group
        print(doctest.testmod())


