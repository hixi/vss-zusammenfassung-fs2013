Zusammenfassung für VSS FS 2013
===============================

Dies ist eine Zusammenfassung für die VSS
Vorlesung, welche im Frühlingssemester 2013
gehalten wurde.

Diese Zusammenfassung wurde mit Hilfe von
Sphinx erstellt. Wie Sphinx benutzt wird
und das (erweiterte) ReST (.rst) Format
kann unter der entsprechenden Anleitung
von Sphinx selbst gefunden werden ->
http://sphinx-doc.org/

Die (aktuell) erstellten PDF/HTML sind unter
dem Verzeichnis ``pdf/`` respektive ``html/``
zu finden.
