Mixed Pickles
=============

CAP Theorem
-----------

**Konsistenz (C)**:
Alle Knoten sehen zur selben Zeit dieselben Daten. Diese
Konsistenz sollte nicht verwechselt werden mit der Konsistenz aus der
ACID-Transaktionen, die nur die innere Konsistenz eines Datenbestandes
betrifft.


**Verfügbarkeit (A)**:
Alle Anfragen an das System werden stets beantwortet.

**Partitionstoleranz (P)**:
Das System arbeitet auch bei Verlust von
Nachrichten, einzelner Netzknoten oder Partition des Netzes weiter.

Da **nur zwei** dieser **drei Anforderungen** in verteilten
Systemen gleichzeitig vollständig erfüllt sein können,
wird das CAP-Theorem oft als Dreieck visualisiert,
bei dem eine konkrete Anwendung sich auf eine der Kanten
einordnen lässt.Die System-Eigenschaften C, A und P können dabei als
graduelle Grössen gesehen werden.


Verteilte Systeme müssen offen sein
-----------------------------------

Verteilte Systeme müssen, ausser in trivialen Fällen, offen sein:

- Offenheit bestimmt, wie gut sich ein System bei neuen Anforderungen anpassen und erweitern lässt.
- Existierende Dienste sollten dabei nicht unterbrochen oder verunmöglicht werden.

Mittel zur Schaffung offener Systeme:

- Schnittstellen
- Publizierte Schnittstellen
- RFC Prozess (Request for Comments)
- Industriestandards
- Internationale Standards (OSI, OMG, ...)
- Einheitliche Kommunikationsmechanismen
- TCP / IP basierte Systeme



Synchrone vs. Asynchrone IPC
----------------------------

+-------------------------------------------+-----------------------------------------------+
| Synchron                                  | Asynchron                                     |
+-------------------------------------------+-----------------------------------------------+
| Sender und Empfänger blockieren beim      | Das Senden findet nicht- blockierend statt    |
| Senden bzw. Empfangen d.h. wenn ein       | d.h. der Sender stellt seine Nachricht in die |
| Senden ausgeführt wird, kann der Sender   | Ausgabe Warteschlange und arbeitet weiter;    |
| erst dann weiterarbeiten, nachdem das     | Das Empfangen kann blockierend oder nicht-    |
| zugehörige „Empfangen“ im Empfänger       | blockierend sein: Blockierend: sobald der     |
| ausgeführt wurde.                         | Empfänger receive() abgesetzt hat wird        |
|                                           | blockiert bis die Nachricht vollständig       |
|                                           | gelesen wurde. Nicht- blockierend: nach       |
|                                           | receive() wird der Buffer gefüllt;            |
|                                           | falls dieser voll ist, wird der               |
|                                           | Empfänger benachrichtigt.                     |
+-------------------------------------------+-----------------------------------------------+
| **Vorteil**                                                                               |
+-------------------------------------------+-----------------------------------------------+
| Leichter zu implementieren                | Effizienter                                   |
+-------------------------------------------+-----------------------------------------------+
| **Nachteil**                                                                              |
+-------------------------------------------+-----------------------------------------------+
| Weniger effizient als Asynchron           | Komplexer als synchron (Queues)               |
+-------------------------------------------+-----------------------------------------------+

.. _pickles_marshalling:

(Un-) Marshalling
.................

Unter **Marshalling** versteht man den Prozess der Transformation
strukturierter Datenelemente und elementarer Werte in eine
(mit einer Nachricht übertragbaren) externe Datendarstellung

Unter **Un-Marshaling** versteht man den Prozess der Erstellung
elementarer Werte aus ihrer externen Datendarstellung und der
Wiederaufbau der Datenstrukturen.


Verteilte Dateisysteme
----------------------

Unterschiedliche Systeme für unterschiedliche Use Cases / Anwendungsfälle:

- Andrew File System
    - Office Bereich
- Hadoop
    - Big Data (Performance Probleme bei wenig Daten)
    - Übung / Musterlösung:
        - Map Reduce

File System Design
..................

**Level 1**

- Ein Benutzer bearbeitet mit einem Prozess lokale Dateien;
- Beispiel: MS/IBM-DOS (Disk Operating System)
- Themen:
    - Namensgebung (für die Dateien und Dateistrukturen)
    - API
    - Mapping der Dateiabstraktion auf phys. Dateien
    - Integrität der Dateien

**Level 2**

- Ein Benutzer bearbeitet mit mehreren Prozessen lokale Dateien;
- Beispiel: OS/2 („half an operating system“)
- Themen
    - Namensgebung (für die Dateien und Dateistrukturen)
    - API
    - Mapping der Dateiabstraktion auf phys. Dateien
    - Integrität der Dateien
    - **Concurrency (Nebenläufigkeiten)**

**Level 3**
 Level 3
- Mehrere Benutzer bearbeiten mit mehreren Prozessen lokale
  Dateien;
- Beispiel: Time-Sharing OS, Unix („tu nix mit Unix“)
- Themen
    - Namensgebung (für die Dateien und Dateistrukturen)
    - API
    - Mapping der Dateiabstraktion auf phys. Dateien
    - Integrität der Dateien
    - Concurrency (Nebenläufigkeiten)
    - **Security**


**Level 4**

- Mehrere Benutzer bearbeiten mit mehreren Prozessen an
  verschiedenen Orten Dateien auf unterschiedlichen Rechnern;
- Beispiel: Cedar (Xerox), DEC RMS, Sun NFS, CMU Andrew und
  Coda File System
- Themen
    - Namensgebung (für die Dateien und Dateistrukturen)
    - API
    - Mapping der Dateiabstraktion auf phys. Dateien
    - Integrität der Dateien
    - Concurrency (Nebenläufigkeiten)
    - Security
    - **File Location und Verfügbarkeit**
      **(zudem Effizienz, Sicherheit und Robustheit)**


Map-Reduce
..........

siehe auch :ref:`dirty_simple_python_map_reduce_src`

Einfaches Beispiel:

Worte (mittels zB Tokenizer) in "Map" einfügen (case Sensitiv im Beispiel,
ohne Satzzeichen),
so dass <*wort*, *wortcount*> bei der ersten Runde (**Map**) heisst alles
<*wort*, 1>:

    Ich gehe gerne fischen, denn fischen mache ich so gerne.

    <Ich, 1>, <gehe, 1>, <gerne, 1>, <fischen, 1>, <denn, 1>, <fischen, 1>,
    <mache, 1>, <ich, 1>, <so, 1>, <gern, 1>

Dann ein Sort/Reduce:
    {'Ich': 1, 'gehe': 1, 'mache': 1, 'ich': 1, 'denn': 1, 'gerne': 2, 'fischen': 2}

in Python kann es etwas so aussehen:

.. code-block:: python

    str_tokenized = "Ich gehe gerne fischen denn fischen mache ich so gerne".split(" ")

    print str_tokenized

    last_word = ""
    counter = 1
    reduced_map = {}
    str_tokenized.sort()
    for word in str_tokenized:
        if last_word == word:
            counter += 1
        else:
            if last_word != "":
                reduced_map[last_word] = counter
                counter = 1
        last_word = word

    print reduced_map

    # Output:
    # ['Ich', 'gehe', 'gerne', 'fischen', 'denn', 'fischen', 'mache', 'ich', 'so', 'gerne']
    # {'Ich': 1, 'gehe': 1, 'mache': 1, 'ich': 1, 'denn': 1, 'gerne': 2, 'fischen': 2}


.. image:: images/mixed_pickles/map_reduce.png

