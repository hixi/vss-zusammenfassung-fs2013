Sockets / API für Internet Protokolle
=====================================

Direkte Netzprogrammierung und Middleware
-----------------------------------------

Direkte Netzprogrammierung
..........................

**Assembler der VS-Programmierung**

- Direkte Kontrolle aller Transportparameter
- grössere Flexibilität bei der Entwicklung neuer Protokolle
- Kann in vielen Fällen bessere Performance bringen

- Grosse Probleme:
    - fehlende Datenrepräsentation
    - Zusatzanforderungen, wie Verschlüsselung, Namensdienst, Transaktionsdienst,
      Fehlerbehandlung etc. müssen „eingekauft“ werden


Middleware
..........

**Höhere Sprache der VS-Programmierung**

- Sehr bequemer Weg zur Entwicklung von Anwendungen
- Datenrepräsentation, Objektlokalisierung, Transaktionsdienst,
  Fehlerbehandlung, Sicherheitsdienst, etc. werden angeboten
  (verschiedene Anbieter).
- Overhead, da allgemein ausgelegt.


Sockets
-------

Übersicht
.........

+----------------------------------------------------------------+----------------------------------------------------------------+
|  .. image:: images/sockets/client_server_modell.png            |  .. image:: images/sockets/client_server_OSI_Layers.png        |
+----------------------------------------------------------------+----------------------------------------------------------------+
|  .. image:: images/sockets/client_server_p2p.png               |  .. image:: images/sockets/client_server_model_schichten.png   |
+----------------------------------------------------------------+----------------------------------------------------------------+

**Synchron** = Blockierend (*send* & *receive*) -> simpler, eifacher
zu implementieren.


**Asynchron** = nicht blockierend (*send* immer, arbeitet weiter
nach *message* in Warteschlange, *receive* kann, muss aber nicht)

UDP Datagramme/TCP Streams
--------------------------

+-------------------------------------------------------------------------------------+------------------------------------------------------------------+
|  .. image:: images/sockets/client_server_besipiel_protokollinteraktion_upd_tcp.png  |  .. image:: images/sockets/client_server_IP_Multicast.png        |
+-------------------------------------------------------------------------------------+------------------------------------------------------------------+
|  .. image:: images/sockets/client_server_tcp_schema.png                             |  .. image:: images/sockets/client_server_udp_schema.png          |
+-------------------------------------------------------------------------------------+------------------------------------------------------------------+
|  .. image:: images/sockets/client_server_IP_Multicast_schema.png                    |  .. image:: images/sockets/client_server_Multicast_txt.png       |
+-------------------------------------------------------------------------------------+------------------------------------------------------------------+


Code Besipiele: :ref:`sockets_src`.


Externe Datendarstellung & Marshalling
--------------------------------------

Marshalling (siehe auch :ref:`pickles_marshalling`) wird gebraucht,
um *Datenobjekte* einheitlich zu Serialisieren. Dies ist darum nötig,
weil unter anderem Big-Endian und Small-Endian Systeme nicht einfach
die gleiche Byte-Reihenfolge umgekehrt werden kann, und schon würde es
wieder stimmen.

Oft reichen **interne** Serialisierungen (*Datenobjekte*) nicht
für einen "guten" Service, daher wird ein Standard-Format
gewählt, zB json oder XML (siehe auch folgenden Abschnitt zu CORBA).


CDR (Common Data Representation) von CORBA
..........................................

RPC wird gebraucht, um auf entfernte Objekte und Daten zu zugreifen,
besser gesagt um mit **entfernten Programmen** und **entfernten**
**Objekten** (Methodenaufrufe) zu kommunizieren.

+-----------------------------------------------+-----------------------------------------------+
| .. image:: images/sockets/RPC_schema.png      | .. image:: images/sockets/RPC_schema2.png     |
+-----------------------------------------------+-----------------------------------------------+
